import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {CurrentWeatherData, ForecastWeatherData} from '../pages/meteo/models/meteo.model';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {
  private API_CURRENT_URL = `${environment.API_URL}${environment.API_CURRENT_OPTION}`;
  private API_FORECAST_URL = `${environment.API_URL}${environment.API_FORECAST_OPTION}`;

  locations: Array<{city: string, country: string}> = [];
    selectLocation: {city: string, country: string} = null;

  constructor(private httpClient: HttpClient) { }

  getCurrent({city, country}: any): Observable<CurrentWeatherData> {
    return this.httpClient.get<{data: CurrentWeatherData}>(`${this.API_CURRENT_URL}&city=${city}&country=${country}${environment.API_KEY}`).pipe(map(res => res.data));
  }

  getForecast({city, country}: any): Observable<Array<ForecastWeatherData>> {
    return this.httpClient.get<{data: ForecastWeatherData[]}>(`${this.API_FORECAST_URL}&city=${city}&country=${country}${environment.API_KEY}`).pipe(map(res => res.data));
  }
}
