import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeteoPage } from './pages/meteo/meteo.page';

const routes: Routes = [
  {
    path: '',
    component: MeteoPage,
    children: [
      {
        path: 'current',
        loadChildren: () => import('./pages/current/current.module').then( m => m.CurrentPageModule)
      },
      {
        path: 'forecast',
        loadChildren: () => import('./pages/forecast/forecast.module').then( m => m.ForecastPageModule)
      },
      {
        path: 'city',
        loadChildren: () => import('../city/city.module').then(m => m.CityPageModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeteoPageRoutingModule {}
