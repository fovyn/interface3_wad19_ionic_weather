import { Component, OnInit } from '@angular/core';
import {MeteoService} from '../../../../services/meteo.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-meteo',
  templateUrl: './meteo.page.html',
  styleUrls: ['./meteo.page.scss'],
})
export class MeteoPage implements OnInit {
  locations: Array<{city: string, country: string}> = [];

  constructor(private meteoService: MeteoService, private router: Router) { }

  ngOnInit() {
    this.locations = this.meteoService.locations;
    console.log(this.meteoService.locations);
  }

  async setLocation($event: CustomEvent) {
    console.log($event.detail.value);
    this.meteoService.selectLocation = $event.detail.value;
    await this.router.navigate(['/meteo/current']);
  }
}
