import { Component, OnInit } from '@angular/core';
import {MeteoService} from '../../../../services/meteo.service';
import {Observable} from 'rxjs';
import {ForecastWeatherData} from '../../models/meteo.model';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.page.html',
  styleUrls: ['./forecast.page.scss'],
})
export class ForecastPage implements OnInit {
  forcast$: Observable<Array<ForecastWeatherData>>;
  constructor(private meteoService: MeteoService) { }

  ngOnInit() {
    this.forcast$ = this.meteoService.getForecast(this.meteoService.selectLocation);
  }

}
