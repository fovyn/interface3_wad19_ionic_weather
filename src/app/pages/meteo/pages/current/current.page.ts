import { Component, OnInit } from '@angular/core';
import {MeteoService} from '../../../../services/meteo.service';
import {Observable} from 'rxjs';
import {CurrentWeatherData} from '../../models/meteo.model';

@Component({
  selector: 'app-current',
  templateUrl: './current.page.html',
  styleUrls: ['./current.page.scss'],
})
export class CurrentPage implements OnInit {
  currentWeather$: Observable<CurrentWeatherData>;
  currentWeather: CurrentWeatherData;

  constructor(private meteoService: MeteoService) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.currentWeather$ = this.meteoService.getCurrent(this.meteoService.selectLocation);
    this.currentWeather$.subscribe(data => {
      this.currentWeather = data[0] as CurrentWeatherData;
    });
  }

  get currentWeatherDetail() {

    const data: Array<{ key: string, value: any }> = [];

    for (const key in this.currentWeather) {
      data.push({key, value: this.currentWeather[key]});
    }

    return data;
  }
}
