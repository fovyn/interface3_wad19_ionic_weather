import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MeteoService} from '../../../services/meteo.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-city-form',
  templateUrl: './create-city-form.component.html',
  styleUrls: ['./create-city-form.component.scss'],
})
export class CreateCityFormComponent implements OnInit {
  form: FormGroup;

  constructor(private builder: FormBuilder, private meteoService: MeteoService, private router: Router) { }

  ngOnInit() {
    this.form = this.builder.group({
      city: new FormControl(''),
      country: new FormControl('')
    });
  }

  createCity() {
    if (this.form.valid) {
      this.meteoService.locations.push(this.form.value);
      this.router.navigate(['/meteo']);
    }
  }
}
