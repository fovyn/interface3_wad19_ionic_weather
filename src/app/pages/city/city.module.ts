import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CityPageRoutingModule } from './city-routing.module';

import { CityPage } from './city.page';
import {MeteoPageModule} from '../meteo/meteo.module';
import {CreateCityFormComponent} from './create-city-form/create-city-form.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CityPageRoutingModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
  declarations: [CityPage, CreateCityFormComponent]
})
export class CityPageModule {}
