import {Injectable} from '@angular/core';
import {CameraPhoto, CameraResultType, CameraSource, Plugins} from '@capacitor/core';

const {Camera, Filesystem, Storage} = Plugins;
@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  pictures: Array<{webViewPath: string}> = [];

  constructor() { }

  async takePhoto() {
    const capturedPhoto: CameraPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 85
    });

    this.pictures.push({webViewPath: capturedPhoto.webPath});
  }
}
